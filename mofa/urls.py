from django.conf.urls import include, url
from django.contrib import admin

from mofa import views


urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),

    url(r'^api/', include('mofa.api.urls')),


    url(r'^ckeditor/', include('ckeditor.urls')),

    url(r'^html/(?P<path>.*)$', views.home, name='home'),

    url(r'^(.*)', views.app, name='app'),
]
