# -*- coding: utf-8 -*-
from django.shortcuts import render


def home(request, path):
    template_name = 'home.html'
    if path:
        template_name = '{}.html'.format(path)
    return render(request, template_name)


def app(request, path=None):
    return render(request, 'app.html')
