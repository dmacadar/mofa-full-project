from django.contrib import admin
from image_cropping import ImageCroppingMixin

from galleries.models import Article


@admin.register(Article)
class ArticleAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('title', 'category', 'subcategory', 'published', 'updated', 'created')
    list_filter = ('category', 'subcategory')

    search_fields = ('title',)
    save_as = True

    date_hierarchy = 'created'

    class Media:
        js = ('js/admin/galleries_article.js',)
